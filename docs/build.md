# Build instructions

Due to licensing restrictions from United Automation, we have split the build process into two steps.
The first step is run by the license holder ([Karl Vestin](mailto:karl.vestin@ess.eu) at the moment),
while the second step can be run by anyone.

## First stage

The first stage is done by the license holder with the wrapper [e3-opcua-build](https://gitlab.esss.lu.se/e3/wrappers/e3-opcua-build). The current configuration of `e3-opcua-build` is set up to expect to be found in the following
directory tree:
```bash
├── ...
├── e3-opcua
├── e3-opcua-build
├── ...
```
The goal of the first stage is to compile the source code into a shared library object (and corresponding `.dbd` file)
which will be committed to the `e3-opcua` wrapper for use by any other developer. The necessary generated files are
* `libopcuabuild.so`
* `opcuabuild.dbd`

In order to generate these, the license holder should do the following standard steps. First, they should set the
correct version in `CONFIG_MODULE`, and then run the following.
```bash
$ make init
$ make build
```
However, instead of running `make install` they should use a special `opcua`-specific target:
```bash
$ make copylibs
```
(See [RULES_MODULES](configure/module/RULES_MODULE)) which will copy the generated files to the `opcua` submodule from the `e3-opcua` wrapper. Once that is done, the license holder should commit those files to that repository:
```bash
$ cd ../e3-opcua/opcua
$ git add opcuaSupport/dbd/opcuabuild.dbd
$ git add opcuaSupport/os/${T_A}/libopcuabuild.so
$ git commit -m "Updated .dbd and .so files"
```
At that point, the license holder should push these changes to [opcua](https://gitlab.esss.lu.se/epics-modules/opcua).
It is probably good to tag this wrapper with an appropriate tag to identify it

## Second stage

The second stage can be done by anyone who would like to install `opcua` to their e3 installation. In order to do this,
you need of course all of the expected e3 build tools, which can be found at [the main e3 page](https://gitlab.esss.lu.se/e3/e3/-/blob/master/README.md). You will further need the `patchelf` utility, and it is also useful to have the
`readelf` utility to confirm that you have patched things correctly. These can be installed via
```bash
$ sudo yum install -y patchelf readelf
```

At this point you can proceed like a usual e3 module: make sure that you are in the `e3-opcua` directory, that you
have set everything correctly in `CONFIG_MODULE` correctly (`EPICS_MODULE_TAG` should match the tag that was set in
the first stage above), and then run
```bash
$ make init
$ make build
$ make install
```

This should install the opcua module correctly in your e3 installation. One should now be able to test it by sourcing
the correct `setE3Env.bash` and running `iocsh.bash`, which should yield output similar to what is below.
```bash
$ iocsh.bash -r opcua
Warning: environment file /home/simonrose/data/git/e3/modules/e3-opcua-test/env.sh does not exist.

registerChannelProviderLocal firstTime true
#
# Start at "2020-W46-Nov10-1319-54-CET"
#
# Version information:
# European Spallation Source ERIC : iocsh.bash (3.3.0-PID-16524)
#
# --->--> snip -->-->
# Please Use Version and other environment variables
# in order to report or debug this shell
#
# HOSTDISPLAY=""
# WINDOWID=""
# PWD="/home/simonrose/data/git/e3/modules/e3-opcua-test"
# USER="simonrose"
# LOGNAME="simonrose"
# EPICS_HOST_ARCH="linux-x86_64"
# EPICS_BASE="/epics/base-7.0.4"
# E3_REQUIRE_NAME="require"
# E3_REQUIRE_VERSION="3.3.0"
# E3_REQUIRE_LOCATION="/epics/base-7.0.4/require/3.3.0"
# E3_REQUIRE_BIN="/epics/base-7.0.4/require/3.3.0/bin"
# E3_REQUIRE_DB="/epics/base-7.0.4/require/3.3.0/db"
# E3_REQUIRE_DBD="/epics/base-7.0.4/require/3.3.0/dbd"
# E3_REQUIRE_INC="/epics/base-7.0.4/require/3.3.0/include"
# E3_REQUIRE_LIB="/epics/base-7.0.4/require/3.3.0/lib"
# EPICS_DRIVER_PATH="/epics/base-7.0.4/require/3.3.0/siteMods:/epics/base-7.0.4/require/3.3.0/siteApps"
# EPICS_CA_AUTO_ADDR_LIST=""
# EPICS_CA_ADDR_LIST=""
# PATH="/epics/base-7.0.4/require/3.3.0/bin:/epics/base-7.0.4/bin/linux-x86_64:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/simonrose/.local/bin:/home/simonrose/bin"
# --->--> snip -->-->
#
# Set REQUIRE_IOC for its internal PVs
epicsEnvSet REQUIRE_IOC "REQMOD:localhost-16524"
#
# Enable an exit subroutine for sotfioc
dbLoadRecords "/epics/base-7.0.4/db/softIocExit.db" "IOC=REQMOD:localhost-16524"
#
# Set E3_IOCSH_TOP for the absolute path where iocsh.bash is executed.
epicsEnvSet E3_IOCSH_TOP "/home/simonrose/data/git/e3/modules/e3-opcua"
#
#
# Load require module, which has the version 3.3.0
#
dlload /epics/base-7.0.4/require/3.3.0/lib/linux-x86_64/librequire.so
dbLoadDatabase /epics/base-7.0.4/require/3.3.0/dbd/require.dbd
require_registerRecordDeviceDriver
Loading module info records for require
#
epicsEnvSet EPICS_DRIVER_PATH /epics/base-7.0.4/require/3.3.0/siteMods:/epics/base-7.0.4/require/3.3.0/siteApps
require opcua
Module opcua version 0.8.0 found in /epics/base-7.0.4/require/3.3.0/opcua/0.8.0/
Loading library /epics/base-7.0.4/require/3.3.0/opcua/0.8.0/lib/linux-x86_64/libopcua.so
Loaded opcua version 0.8.0
Loading dbd file /epics/base-7.0.4/require/3.3.0/opcua/0.8.0/dbd/opcua.dbd
Calling function opcua_registerRecordDeviceDriver
Loading module info records for opcua
# Set the IOC Prompt String One
epicsEnvSet IOCSH_PS1 "localhost-16524 > "
#
#
iocInit
Starting iocInit
############################################################################
## EPICS R7.0.4-E3-7.0.4-patch
## Rev. 2020-07-02T14:38+0200
############################################################################
OPC UA Client Device Support 0.8.0-dev (@EPICS_OPCUA_GIT_COMMIT@); using Unified Automation C++ Client SDK v1.7.1-476
iocRun: All initialization complete
localhost-16524 >
```
