import getopt
import sys
import xml.etree.ElementTree as ET

# Dictionary to convert OPCUA data types to EPISC record types
type_dictionary = {
    "VariantType.Double": "ai",
    "VariantType.Float": "ai",
    "VariantType.Boolean": "bi",
    "VariantType.String": "stringin",
    "VariantType.Int64": "int64in",
    "VariantType.UInt64": "int64in",
    "VariantType.Int32": "longin",
    "VariantType.UInt32": "longin",
    "VariantType.StatusCode": "longin",
    "Double": "ai",
    "Float": "ai",
    "Boolean": "bi",
    "String": "stringin",
    "Int64": "int64in",
    "UInt64": "int64in",
    "Int32": "longin",
    "UInt32": "longin",
    "StatusCode": "longin",
}

# Default values
input_file = "nodeset.xml"
output_file = "output_xml.subst"
template_file = "opcua_pv_xml.template"
ns = "{http://opcfoundation.org/UA/2011/03/UANodeSet.xsd}"

# Get arguments
try:
    opts, args = getopt.getopt(
        sys.argv[1:],
        "i:o:t:n:",
        ["ifile=", "ofile=", "tfile=", "-namespace="],
    )

# Exit if incorrect arguments are given
except getopt.GetoptError:
    print(
        "opcua_xml_parser.py -i <input file> -o <output file> -t <template file> -n <name space>"
    )
    sys.exit(2)

# Override default with arguments
for opt, arg in opts:
    if opt in ("-o", "--ofile"):
        output_file = arg

    elif opt in ("-t", "--tfile"):
        template_file = arg

    elif opt in ("-i", "--ifile"):
        input_file = arg

    elif opt in ("-n", "--namespace"):
        ns = arg

# Open the root node of the input XML schema file
mytree = ET.parse(input_file)
myroot = mytree.getroot()

# Header of substitution file
lines = []
lines.append("file " + template_file + " {\n")
lines.append("pattern {RECORD_TYPE,PROPERTY,NODE_ID}\n")

# Loop through all variable nodes in the XML node set
for x in myroot.findall(ns + "UAVariable"):

    # Find the EPICS record type by lookup in the dictionary
    record_type = type_dictionary.get(
        next(item for item in x.items() if item[0] == "DataType")[1], "ai"
    )

    # Find OPCUA node id from XML node set file
    node_id = next(item for item in x.items() if item[0] == "NodeId")[1]

    # Generate a property name based on the browse name. This will typically need to be changed afterwards
    property_name = ":" + next(item for item in x.items() if item[0] == "BrowseName")[1]

    # Create a line in teh substitution file to eventually generate the data base record (via database expansion)
    lines.append("{" + record_type + "," + property_name + "," + node_id + '"}\n')

# Footer of the substitution file
lines.append("}")

# Write the file to disk
with open(output_file, "w") as f:
    f.writelines(lines)
