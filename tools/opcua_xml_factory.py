import sys

from opcua.common.xmlexporter import XmlExporter
from progress.bar import Bar
from progress.counter import Counter

from opcua import Client


class MyXmlExporter(XmlExporter):
    def build_etree(self, node_list, uris=None):
        """
        Create an XML etree object from a list of nodes; custom namespace uris are optional
        Namespaces used by nodes are always exported for consistency.
        Args:
            node_list: list of Node objects for export
            uris: list of namespace uri strings
        Returns:
        """
        self.logger.info("Building XML etree")

        self._add_namespaces(node_list, uris)

        # add all nodes in the list to the XML etree
        bar = Bar("Processing", max=len(node_list))
        for node in node_list:
            self.node_to_etree(node)
            bar.next()
        bar.finish()

        # add aliases to the XML etree
        self._add_alias_els()


# Recursive function that loops through all nodes in the OPCUA hierachy and returns a flat list of all Variable nodes (i.e. nodes with data)
def traverse_nodes(node, spinner):
    if str(node.get_node_class()) == "NodeClass.Variable":
        nodes = [node]
    else:
        nodes = []
    # Iterate over all referenced nodes (31), only hierarchical references (33)
    for child in node.get_children(refs=33):
        if child not in nodes:
            spinner.next()
            nodes = nodes + traverse_nodes(child, spinner)

    return nodes


# Default values
server_url = "opc.tcp://localhost:4840"
output_file = "output_xml.xml"
namespaces = [2]

# Connect to the client
client = Client(server_url)
try:
    client.connect()
except Exception as e:
    print("Could not connect to " + server_url)
    print(str(e))
    sys.exit()

# Create node list and filter on name space
spinner = Counter("Fetching nodes...")
nodes = traverse_nodes(client.get_objects_node(), spinner)
spinner.finish()
if namespaces:
    nodes = [node for node in nodes if namespaces.count(node.nodeid.NamespaceIndex) > 0]

answer = input(
    "{} variable type nodes collected. Continue, this may take some time (Y/n)?".format(
        len(nodes)
    )
)
if answer.upper() not in ["N", "NO"]:
    # Export the node set XML file
    exp = MyXmlExporter(client)
    exp.build_etree(nodes)
    exp.write_xml(output_file)

# Disconnect client
client.disconnect()
