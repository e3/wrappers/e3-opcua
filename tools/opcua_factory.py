import getopt
import sys

from opcua import Client


def TraverseVariables(node, blacklist):
    allvariables = []
    nodeName = str(node)
    notblacklisted = True
    if nodeName in blacklist:
        notblacklisted = False

    if node and notblacklisted:
        variables = node.get_variables()
        allvariables.extend(variables)
        children = node.get_children()
        for child in children:
            allvariables = allvariables + TraverseVariables(child, blacklist)
        return allvariables
    return []


def getPVName(nodepath):
    return nodepath


def getDataType(opcType):
    # Unmapped data types
    # SByte	char
    # Byte	unsigned char
    # Int16	short
    # UInt16	unsigned short
    # DateTime	uint64_t
    # ByteString	string (eg group of channels of different type)
    # XmlElement	string (eg group of channels of different type)

    # Mapped data types
    # OPCUA data type         # EPICS record types
    dictionary = {
        "VariantType.Double": "ai",
        "VariantType.Float": "ai",
        "VariantType.Boolean": "bi",
        "VariantType.String": "stringin",
        "VariantType.Int64": "int64in",
        "VariantType.UInt64": "int64in",
        "VariantType.Int32": "longin",
        "VariantType.UInt32": "longin",
        "VariantType.StatusCode": "longin",
    }
    return dictionary.get(str(opcType), "unknown")


outputfile = ""
custompvfile = ""
host = ""
templatefile = "opcua_pv.template"
blacklistfile = ""
subscription = "SUBS"

try:
    opts, args = getopt.getopt(
        sys.argv[1:],
        "p:o:s:t:b:u:",
        ["pfile=", "ofile=", "surl=", "-tfile=", "-bfile=", "-usub"],
    )
except getopt.GetoptError:
    print("test.py -p <custompvnamefile> -o <outputfile>")
    sys.exit(2)

for opt, arg in opts:
    if opt in ("-p", "--pfile"):
        custompvfile = arg
        get_pv_name = __import__(custompvfile, globals(), locals())
    elif opt in ("-o", "--ofile"):
        outputfile = arg
    elif opt in ("-s", "--surl"):
        host = arg
    elif opt in ("-t", "--tfile"):
        templatefile = arg
    elif opt in ("-b", "--bfile"):
        blacklistfile = arg
        blacklistmod = __import__(blacklistfile, globals(), locals())
    elif opt in ("-u", "--usub"):
        subscription = arg

if host == "":
    host = "localhost:4840"

client = Client("opc.tcp://" + host)
try:
    client.connect()
except Exception:
    print("Could not connect to " + host)
    sys.exit(2)

blacklist = []
if blacklistfile != "":
    blacklist = blacklistmod.get_blacklist()

variables = TraverseVariables(client.get_objects_node(), blacklist)
lines = []
lines.append("file " + templatefile + " {\n")
lines.append("pattern {RECORD_TYPE,PV_NAME,SUBS,NODE_ID,DESC}\n")

for variable in variables:
    nodeId = str(variable).replace("\\", "\\\\")
    pvName = nodeId
    if custompvfile != "":
        pvName = get_pv_name.getPVName(variable.get_path())
    record_type = str(getDataType(variable.get_data_type_as_variant_type()))
    desc = str(variable.get_description().Text)
    lines.append(
        "{"
        + record_type
        + ","
        + pvName
        + ","
        + subscription
        + ","
        + nodeId
        + ',"'
        + desc
        + '"}\n'
    )

lines.append("}")

if outputfile == "":
    for line in lines:
        print(line.rstrip())
else:
    with open(outputfile, "w") as f:
        f.writelines(lines)

client.disconnect()
