def getPVName(nodepath):
    nodeid = nodepath[-1].nodeid.to_string()
    idparts = nodeid.split("\\")
    if len(idparts) < 2:
        return ""

    # "Pwr-$(SUBSTATION)$(SLOT):CnPw-Q-001:CurrentL1-RB")
    # field( INP, "@$(SUBS) ns=$(NAMESPACE);s=0:\\\\APL\\1\\P\\$(SUBSTATION)_$(SLOT)_M\\10")

    pv = "Pwr-{}{}:CnPw-Q001:{}"
    subst = idparts[-2].split("_")[0]
    slot = idparts[-2].split("_")[1]
    prop = "CurrentL1-RB"
    pv = pv.format(subst, slot, prop)
    return pv
