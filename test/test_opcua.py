import os
import re
import resource
import signal
import subprocess
import time
import logging
from datetime import datetime
from os import environ
from time import sleep

import pytest
from epics import PV, ca
from run_iocsh import IOC
from opcua import Client

logger = logging.getLogger()
logging.basicConfig(format="%(levelname)s:%(message)s")


class opcuaTestHarness:
    def __init__(self):

        # Get values from the environment
        self.MOD_VERSION = environ.get("E3_MODULE_VERSION", "0.9.3")
        self.TEMP_CELL_PATH = environ.get("TEMP_CELL_PATH", "cellMods")

        self.TestArgs = [
            "-l",
            self.TEMP_CELL_PATH,
            "-r",
            f"opcua,{self.MOD_VERSION}",
        ]

        self.cmd = "test/cmds/test_pv.cmd"

        # Default IOC
        self.IOC = self.get_ioc()

        # timeout value in seconds for pvput/pvget calls
        self.timeout = 5
        self.putTimeout = self.timeout
        self.getTimeout = self.timeout

        # test sleep time ins seconds
        self.sleepTime = 3

        # Test server
        self.testServer = "test/server/opcuaTestServer"
        self.isServerRunning = False
        self.serverURI = "opc.tcp://localhost.localdomain:4840"
        self.serverFakeTime = "2019-05-02 09:22:52"
        _templateMsg = "OPC UA session OPC1: connection status changed from {0} to {1}"

        # Message catalog
        self.connectMsg = _templateMsg.format("Disconnected", "Connected")
        self.reconnectMsg = _templateMsg.format(
            "ConnectionErrorApiReconnect", "NewSessionCreated"
        )
        self.reconnectMsg1 = _templateMsg.format("NewSessionCreated", "Connected")
        self.noConnectMsg = _templateMsg.format(
            "ConnectionErrorApiReconnect", "NewSessionCreated"
        )

        self.badNodeIdMsg = "item ns=2;s=Sim.BadVarName : BadNodeIdUnknown"

        # Server variables
        self.serverVars = [
            "open62541",
            "open62541 OPC UA Server",
            "1.2.0-29-g875d33a9",
        ]

    def get_ioc(self):
        return IOC(
            *self.TestArgs,
            self.cmd,
        )

    def start_server(self, withPIPE=False):
        if withPIPE:
            self.serverProc = subprocess.Popen(
                self.testServer,
                stdout=subprocess.PIPE,
            )
        else:
            self.serverProc = subprocess.Popen(
                self.testServer,
            )

        logger.info("Opening server with pid = %s" % self.serverProc.pid)
        retryCount = 0
        while (not self.isServerRunning) and retryCount < 5:
            self.is_server_running()
            retryCount = retryCount + 1
            sleep(1)

        sleep(1)
        assert retryCount < 5, "Unable to start server"

    def start_server_with_faketime(self):
        self.serverProc = subprocess.Popen(
            "faketime -f '%s' %s" % (self.serverFakeTime, self.testServer),
            shell=True,
            preexec_fn=os.setsid,
        )

        logger.info("Opening server with pid = %s" % self.serverProc.pid)
        retryCount = 0
        while (not self.isServerRunning) and retryCount < 5:
            self.is_server_running()
            retryCount = retryCount + 1
            sleep(1)

        sleep(1)
        assert retryCount < 5, "Unable to start server"

    def stop_server_group(self):
        # Get the process group ID for the spawned shell,
        # and send terminate signal
        logger.info("Closing server group with pgid = %s" % self.serverProc.pid)
        os.killpg(os.getpgid(self.serverProc.pid), signal.SIGTERM)
        self.is_server_running()

    def stop_server(self):
        logger.info("Closing server with pid = %s" % self.serverProc.pid)
        self.serverProc.terminate()
        self.serverProc.wait(timeout=5)
        self.is_server_running()

    def is_server_running(self):
        c = Client(self.serverURI)
        try:
            c.connect()
            # NS0|2259 is the server state variable
            # 0 -- Running
            var = c.get_node("ns=0;i=2259")
            val = var.get_data_value()
            self.isServerRunning = val.StatusCode.is_good()
            c.disconnect()

        except Exception:
            self.isServerRunning = False


# Standard test fixture
@pytest.fixture(scope="function")
def test_inst():
    """
    Instantiate test harness, start the server,
    yield the harness handle to the test,
    close the server on test end / failure
    """
    ca.initialize_libca()

    test_inst = opcuaTestHarness()
    test_inst.is_server_running()
    assert not (
        test_inst.isServerRunning
    ), "An instance of the OPC-UA test server is already running"
    test_inst.start_server()
    yield test_inst

    test_inst.stop_server()
    assert not test_inst.isServerRunning

    ca.flush_io()
    ca.clear_cache()


# test fixture for use with timezone server
@pytest.fixture(scope="function")
def test_inst_TZ():
    """
    Instantiate test harness, start the server,
    yield the harness handle to the test,
    close the server on test end / failure
    """
    ca.initialize_libca()

    test_inst_TZ = opcuaTestHarness()
    test_inst_TZ.is_server_running()
    assert not (
        test_inst_TZ.isServerRunning
    ), "An instance of the OPC-UA test server is already running"
    test_inst_TZ.start_server_with_faketime()
    yield test_inst_TZ

    test_inst_TZ.stop_server_group()
    assert not test_inst_TZ.isServerRunning

    ca.flush_io()
    ca.clear_cache()


class TestConnectionTests:

    # Positive Test Cases
    def test_connect_disconnect(self, test_inst):
        """
        Connect and disconnect to the OPC-UA test server, and
        check that there are no errors
        """

        ioc = test_inst.IOC
        nRuns = 5

        for i in range(0, nRuns):
            ioc.start()
            assert ioc.is_running()

            ioc.exit()
            assert not ioc.is_running()

            ioc.check_output()
            stderr = ioc.errs

            # Parse for OPC-UA connection message
            assert (
                stderr.find(test_inst.connectMsg) >= 0
            ), "%d: Failed to find disconnect message\n%s" % (i, stderr)

            logger.debug(stderr)

    def test_connect_reconnect(self, test_inst):
        """
        Start the server, start the IOC. Stop the server, check
        for appropriate messaging. Start the server, check that
        the IOC reconnects.
        """
        ioc = test_inst.IOC

        nRuns = 5

        for i in range(0, nRuns):
            ioc.start()
            assert ioc.is_running()

            test_inst.stop_server()
            assert ioc.is_running()

            sleep(test_inst.sleepTime)

            test_inst.start_server()
            assert ioc.is_running()

            sleep(test_inst.sleepTime)

            ioc.exit()
            assert not ioc.is_running()

        ioc.check_output()
        stderr = ioc.errs
        logger.debug(stderr)

        # Parse for OPC-UA connection message
        assert (
            stderr.find(test_inst.reconnectMsg) >= 0
        ), "%d: Failed to find reconnect message\n%s" % (i, stderr)
        assert (
            stderr.find(test_inst.reconnectMsg1) >= 0
        ), "%d: Failed to find reconnect message 1\n%s" % (i, stderr)

    def test_no_connection(self, test_inst):
        """
        Start an IOC with no server running. Check the module
        reports this.
        """

        ioc = test_inst.IOC

        test_inst.stop_server()

        ioc.start()
        assert ioc.is_running()

        sleep(test_inst.sleepTime)

        test_inst.start_server()

        sleep(test_inst.sleepTime)

        ioc.exit()
        assert not ioc.is_running()

        ioc.check_output()
        stderr = ioc.errs
        logger.debug(stderr)

        i = 1
        # Parse for OPC-UA connection message
        assert (
            stderr.find(test_inst.noConnectMsg) >= 0
        ), "%d: Failed to find no connection message\n%s" % (i, stderr)
        assert (
            stderr.find(test_inst.reconnectMsg) >= 0
        ), "%d: Failed to find reconnect message in output\n%s" % (i, stderr)
        assert (
            stderr.find(test_inst.reconnectMsg1) >= 0
        ), "%d: Failed to find reconnect message 1 in output\n%s" % (i, stderr)

    def test_shutdown_on_ioc_reboot(self, test_inst):
        """
        Start the server. Start an IOC and ensure connection
        is made to the server. Shutdown the IOC and endure
        that the subscriptions and sessions are cleanly
        disconnected.
        """
        # Close connection to server, and open new connection
        # with stdout PIPE
        test_inst.stop_server()
        test_inst.start_server(withPIPE=True)

        ioc = test_inst.IOC

        ioc.start()
        assert ioc.is_running()
        # Wait a second to allow it to get up and running
        sleep(1)

        ioc.exit()
        assert not ioc.is_running()
        # Wait for it to close down
        sleep(1)

        # Shutdown the server to allow us to get the stdout messages
        test_inst.stop_server()

        # Read all lines from the stdout buffer
        log = ""
        for line in iter(test_inst.serverProc.stdout.readline, b""):
            log = log + line.decode("utf-8")

        logger.debug(log)

        assert log.find("ActivateSession: Session activated") >= 0, (
            "Failed to find ActivateSession message: %s" % log
        )
        assert log.find("Subscription 1 | Created the Subscription") >= 0, (
            "Failed to find Subscription message: %s" % log
        )

        # Find the position in the log where the terminate signal
        # is received
        termPos = log.find("received ctrl-c")

        # Check that the session and subscription close messages
        # occur before the terminate signal is received.
        # This means they were closed when the IOC was shutdown
        closePos = log.find("Closing the Session")
        assert 0 <= closePos <= termPos, (
            "Session closed by terminate, not by IOC shutdown: %s" % log
        )

        ioc.check_output()
        output = ioc.outs
        logger.debug(output)


class TestVariableTests:
    def test_server_status(self, test_inst):
        """
        Check the informational values provided by the server
        are being translated via the module
        """
        ioc = test_inst.IOC

        serverVars = [
            "OPC:ServerManufacturerName",
            "OPC:ServerProductName",
            "OPC:ServerSoftwareVersion",
        ]
        i = 0
        with ioc:
            sleep(1)
            for pvName in serverVars:
                pv = PV(pvName)
                res = pv.get(timeout=test_inst.getTimeout)
                assert res == test_inst.serverVars[i]
                i = i + 1

    def test_variable_pvget(self, test_inst):
        """
        Variable on the OPCUA server increments by 1 each second
        Sample the value using pvget once a second and check the
        value is incrmenting.
        """

        with IOC(
            *test_inst.TestArgs,
            test_inst.cmd,
        ):
            sleep(1)
            pvName = "TstRamp"
            pv = PV(pvName)

            # Test parameters
            minVal = 0  # Ramp min, as defined by server
            maxVal = 1000  # Ramp max, as defined by server
            captureLen = 5  # Number of samples to capture
            captureIncr = 5  # Time increment in seconds

            res = [int(0)] * captureLen
            resCheck = [int(0)] * captureLen

            for i in range(0, captureLen):
                res[i] = int(pv.get(timeout=test_inst.getTimeout))
                sleep(captureIncr)

            wrapOffset = 0
            resCheck[0] = res[0]
            # Check ramp is incrementing correctly
            for i in range(1, captureLen):
                # Handle possible wraparound from 1000 -> 0
                if res[i] == minVal:
                    wrapOffset = maxVal + 1
                resCheck[i] = res[i] + wrapOffset
                expected = resCheck[i - 1] + captureIncr
                logger.debug(
                    "Captured value (%d) is %d. Expected %d +/-1"
                    % (
                        i,
                        res[i],
                        expected,
                    )
                )
                assert (
                    expected - 1 <= resCheck[i] <= expected + 1
                ), "Captured value (%d) is %d. Expected %d +/-1" % (
                    i,
                    res[i],
                    expected,
                )

    @pytest.mark.parametrize(
        "pvName,expectedVal",
        [
            ("VarCheckBool", True),
            ("VarCheckSByte", -128),
            ("VarCheckByte", 255),
            ("VarCheckInt16", -32768),
            ("VarCheckUInt16", 65535),
            ("VarCheckInt32", -2147483648),
            ("VarCheckUInt32", 4294967295),
            ("VarCheckInt64", -1294967296),
            ("VarCheckUInt64", "{:.16e}".format(18446744073709551615)),
            ("VarCheckFloat", -0.0625),
            ("VarCheckDouble", 0.002),
            ("VarCheckString", "TestString01"),
        ],
    )
    def test_read_variable(self, test_inst, pvName, expectedVal):
        """
        Read the deafult value of a variable from the opcua
        server and check it matches the expected value.
        Parametrised for all supported datatypes.
        """
        ioc = test_inst.IOC

        with ioc:
            sleep(1)
            pv = PV(pvName)
            res = pv.get(timeout=test_inst.getTimeout)
            # Check UInt64 with correct scientific notation
            if pvName == "VarCheckUInt64":
                res = "%.16e" % res
            assert res == expectedVal

    @pytest.mark.parametrize(
        "pvName,writeVal",
        [
            ("VarCheckBool", False),
            ("VarCheckSByte", 127),
            ("VarCheckByte", 128),
            ("VarCheckInt16", 32767),
            ("VarCheckUInt16", 32768),
            ("VarCheckInt32", 2147483647),
            ("VarCheckUInt32", 2147483648),
            ("VarCheckInt64", 0),
            ("VarCheckUInt64", 0),
            ("VarCheckFloat", -0.03125),
            ("VarCheckDouble", -0.004),
            ("VarCheckString", "ModifiedTestString"),
        ],
    )
    def test_write_variable(self, test_inst, pvName, writeVal):
        """
        Write a known value to the opcua server via the
        output PV linked to the variable. Read back via
        the input PV and check the values match.
        Parametrised for all supported datatypes.
        """
        ioc = test_inst.IOC

        with ioc:
            sleep(1)
            # Output PV name is the same as the input PV
            # name, with the addition of the "Out" suffix
            pvOutName = pvName + "Out"
            pvWrite = PV(pvOutName)
            assert ioc.is_running()
            assert (
                pvWrite.put(writeVal, wait=True, timeout=test_inst.putTimeout)
                is not None
            ), ("Failed to write to PV %s\n" % pvOutName)

            # Wait 1s to ensure write has time to pass through
            # asynchronous layers
            sleep(1)

            pvRead = PV(pvName)
            assert ioc.is_running()
            res = pvRead.get(use_monitor=False, timeout=test_inst.getTimeout)
            retryCnt = 0
            while res is None:
                logger.debug("%d: Read timeout. Try again...\n" % retryCnt)
                ioc.exit()
                ioc.start()
                res = pvRead.get(
                    use_monitor=False, timeout=test_inst.getTimeout
                )  # NoQA: E501
                if retryCnt > 3:
                    break

            assert res == writeVal

    def test_timestamps(self, test_inst_TZ):
        """
        Start the test server in a shell session with
        with a fake time in the past. Check that the
        timestamp for the PV read matches the known
        fake time given to the server.
        If they match, the OPCUA EPICS module is
        correctly pulling the timestamps from the
        OPCUA server (and not using a local TS)
        """

        ioc = test_inst_TZ.IOC

        with ioc:
            sleep(1)
            pvName = PV("TstRamp")
            timevars = pvName.get_timevars()
            epicsTs = timevars["timestamp"]

        form = "%Y-%m-%d %H:%M:%S"
        pyTs = datetime.strptime(test_inst_TZ.serverFakeTime, form).timestamp()

        assert epicsTs == pyTs, "Timestamp returned does not match"


class TestPerformanceTests:
    @pytest.mark.xfail("CI" in environ, reason="GitLab runner performance issues")
    def test_write_performance(self, test_inst):
        """
        Write 5000 variable values and measure
        time and memory consumption before
        and after. Repeat 10 times
        """
        ioc = test_inst.IOC

        with ioc:
            sleep(1)
            pvWrite = PV("VarCheckInt16Out")

            assert ioc.is_running()

            maxt = 0
            mint = float("inf")
            tott = 0
            totr = 0
            testruns = 10
            writeperrun = 5000

            # Run test 10 times
            for j in range(1, testruns):

                # Get time and memory conspumtion before test
                r0 = resource.getrusage(resource.RUSAGE_THREAD)
                t0 = time.perf_counter()

                # Write 5000 PVs
                for i in range(1, writeperrun):
                    pvWrite.put(i, wait=True, timeout=test_inst.putTimeout)

                # Get delta time and delta memory
                dt = time.perf_counter() - t0
                r1 = resource.getrusage(resource.RUSAGE_THREAD)
                dr = r1.ru_maxrss - r0.ru_maxrss  # NoQA: E501

                # Collect data for statistics
                if dt > maxt:
                    maxt = dt
                if dt < mint:
                    mint = dt
                tott += dt
                totr += dr

            avgt = tott / testruns

            assert maxt < 17
            assert mint > 0.8
            assert avgt < 5
            assert totr < 3000

    def test_read_performance(self, test_inst):
        """
        Read 5000 variable values and measure time and
        memory consumption before and after.
        Repeat 10 times
        """
        ioc = test_inst.IOC

        with ioc:
            sleep(1)
            pvRead = PV("VarCheckInt16")

            assert ioc.is_running()

            maxt = 0
            mint = float("inf")
            tott = 0
            totr = 0
            testruns = 10
            writeperrun = 5000

            # Run test 10 times
            for j in range(1, testruns):

                # Get time and memory conspumtion before test
                r0 = resource.getrusage(resource.RUSAGE_SELF)
                t0 = time.perf_counter()

                # Read 5000 PVs
                for i in range(1, writeperrun):
                    pvRead.get(timeout=test_inst.putTimeout)

                # Get delta time and delta memory
                dt = time.perf_counter() - t0
                r1 = resource.getrusage(resource.RUSAGE_SELF)
                dr = r1.ru_maxrss - r0.ru_maxrss  # NoQA: E501

                # Collect data for statistics
                if dt > maxt:
                    maxt = dt
                if dt < mint:
                    mint = dt
                tott += dt
                totr += dr
                logger.debug("Time: ", dt)
                logger.debug("Memory: ", dr)
                logger.debug("Memory: ", r0.ru_maxrss)
                logger.debug("Memory: ", r1.ru_maxrss)
            avgt = tott / testruns

            logger.info("Max time: ", maxt)
            logger.info("Min time: ", mint)
            logger.info("Average time: ", avgt)
            logger.info("Total memory: ", totr)

            assert maxt < 10
            assert mint > 0.01
            assert avgt < 7
            assert totr < 1000


class TestNegativeTests:
    def test_no_server(self, test_inst):
        """
        Start an OPC-UA IOC with no server running.
        Check the module reports this correctly.
        """

        ioc = test_inst.IOC

        test_inst.stop_server()

        ioc.start()
        assert ioc.is_running()

        sleep(1)

        # Check that PVs have SEVR INVALID (=3)
        pv = PV("VarCheckSByte")
        pv.get(timeout=test_inst.getTimeout)
        assert pv.severity == 3

        ioc.exit()
        assert not ioc.is_running()

        ioc.check_output()
        output = ioc.outs
        logger.debug(output)

    def test_bad_var_name(self, test_inst):
        """
        Specify an incorrect variable name in a db record.
        Start the IOC and verify a sensible error is
        displayed.
        """

        test_inst.cmd = "test/cmds/test_pv_neg.cmd"
        ioc = test_inst.get_ioc()

        ioc.start()
        assert ioc.is_running()

        sleep(1)

        ioc.exit()
        assert not ioc.is_running()

        ioc.check_output()
        stderr = ioc.errs
        logger.debug(stderr)

        assert stderr.find(test_inst.badNodeIdMsg) >= 0, (
            "Failed to find BadNodeIdUnknown message\n%s" % stderr
        )

    def test_wrong_datatype(self, test_inst):
        """
        Specify an incorrect record type for an OPC-UA variable.
        Binary input record for a float datatype.
        """

        test_inst.cmd = "test/cmds/test_pv_neg.cmd"
        ioc = test_inst.get_ioc()

        ioc.start()
        assert ioc.is_running()

        sleep(1)

        ioc.exit()
        assert not ioc.is_running()

        ioc.check_output()
        stderr = ioc.errs
        logger.debug(stderr)

        regx = "VarNotBoolean : incoming data (.*) out-of-bounds"
        assert re.search(regx, stderr)
